"""
Utility functions for processing IMOS data
"""

from datetime import datetime
import numpy as np
import pandas as pd
import xarray as xr

from scipy.interpolate import PchipInterpolator, interp1d
import scipy.linalg as la
from scipy.optimize import newton_krylov

import pdb

GRAV = 9.81
RHO0 = 1024.

def convert_time(tt):
    try:
        dt= datetime.strptime(tt, '%Y-%m-%dT%H:%M:%S')
    except:
        dt= datetime.strptime(tt, '%Y-%m-%d %H:%M')
    return dt

def read_density_csv(csvfile):
    # Reads into a dataframe object
    df = pd.read_csv(csvfile, index_col=0, sep=', ', parse_dates=['Time'], date_parser=convert_time)

    # Load the csv data
    depths= np.array([float(ii) for ii in df.columns.values])
    rho_obs_tmp = df[:].values.astype(float)
    time = df.index[:]

    # Clip the top
    rho_obs_2d = rho_obs_tmp[:,:]

    # Remove some nan
    fill_value = 1024.
    rho_obs_2d[np.isnan(rho_obs_2d)] = fill_value
    
    return xr.DataArray(rho_obs_2d,dims=('time', 'depth'),
            coords={'time':time.values,'depth':depths})

def fit_bmodes_linear_wgaps(y, z,  zmin, modes, iw,  Nz=100):
    """
    Compute the linear modal amplitude to the mode numbers in the list

    Inputs:
    ---
        y - matrix[nz, nt], density data
        iw - IWaveMode class
        z - vector[nz], depth from bottom to top, negative values (ascending)
        modes - list[nmodes], mode numbers in python index i.e. 0=1

    Returns
    ---
        A_t - matrix[nmodes, nt], modal amplitude
        ...

    """

    # Compute buoyancy from density and backgroud density
    #rhopr = rho.T - iw.rho[np.newaxis,...]
    #y = GRAV*rhopr/RHO0

    nmodes = len(modes)
    nz, nt = y.shape

    # Compute the modal structures
    Lout = np.ones((Nz,nmodes))
    phi_n = np.ones((Nz,nmodes))

    # Calculate dz
    Z = np.linspace(zmin, 0, Nz)
    dz = np.mean(np.diff(Z))

    c1 = []
    r10 = []

    for ii, mode in enumerate(modes):
        # Use the mode class to create the profile
        phi, cn, he, znew = iw(zmin, dz, mode)
        try:
            rn0, _, _, _ = iw.calc_nonlin_params()
        except:
            print('Nonlinear parameter calculation failed.')
            rn0 = 0.

        c1.append(cn)
        r10.append(rn0)

        #iw.plot_modes()
        #plt.show()

        #Lout[:,ii] = phi[::-1]*iw.N2[::-1]
        Lout[:,ii] = phi[::-1]*iw.N2[::-1]
        phi_n[:,ii] = phi # We want this in the original direction
        #Lout[:,ii] = phi*iw.N2

    # Compute the modal shape for each time step
    A_t = np.zeros((nt,nmodes))
    for tt in range(nt):
        #for tt in [0]:
        # Quality control the data
        ytmp = y[:,tt]
        idx = ~np.isnan(ytmp)
        
        ## Interpolate the modal shape and N2 onto the measured depth locations
        #F = PchipInterpolator(znew[::-1], Lout, axis=0)
        F = interp1d(znew[::-1], Lout, axis=0, kind='linear',\
                bounds_error=False, fill_value='extrapolate')
        L = F(z[idx])

        # Compute density to buoyancy perturbation
        #F = PchipInterpolator(znew[::-1], iw.rho, axis=0)
        F = interp1d(znew[::-1], iw.rhoZ[::-1], axis=0, kind='linear',\
                bounds_error=False, fill_value='extrapolate')
        rhobar = F(z[idx])
        b = GRAV*(ytmp[idx] - rhobar)/RHO0

        # Find how many bad points
        numbad = float((idx==False).sum())
        #pbad = numbad/float(len(idx))
        pbad = numbad/nz
        if  pbad > 0.15:
            #print('Skipping fit too many bad points (%f)'%pbad*100)
            A_tmp = np.zeros((nmodes,))
        else:
            ## Fit Ax=b
            try:
                A_tmp,_,_,_ = la.lstsq(L , b)
            except:
                print('Fitting failed!')
                A_tmp = np.zeros((nmodes,))

        A_t[tt,:] = A_tmp
        #print A_tmp[0]

    bfit_n = Lout[:,np.newaxis,:]*A_t[np.newaxis,...]
    bfit = bfit_n.sum(axis=-1) # sum the modes
    rhofit = bfit.T*RHO0/GRAV + iw.rhoZ[np.newaxis,::-1]

    return A_t, rhofit[:,::-1], phi_n, np.array(c1), np.array(r10)

def qc_temp(T, fac=3, minT=2.0, maxT=37.0):
    mask = T.y.mask.copy()
    tmean = np.median(T.y, axis=1)
    tstd = T.y.std(axis=1)
    idx = (T.y.T > tmean + tstd*fac) & (T.y.T > tmean - tstd*fac) 
    mask[idx.T] = True

    idx2 = T.y < minT
    mask[idx2] = True

    idx3 = T.y > maxT
    mask[idx3] = True

    T.y[mask] = np.nan
    T.y = np.ma.MaskedArray(T.y, mask=mask)


