# -*- coding: utf-8 -*-
"""
Update or create a database with point observation netcdf files
"""

from soda.dataio import netcdfio

####
# Inputs
create = False
#create = True

basedir = '/home/suntans/Share/ARCHub/DATA/FIELD/RowleyShoals2019'

# This could be an existing database
dbfile = '%s/RowleyShoals2019_Obs.db'%basedir
   
ncfiles = [\
    #'%s/processed_data/RowleyShoals_SBE37Data.nc'%basedir,\
    #'%s/processed_data/RowleyShoals_SBE39Data.nc'%basedir,\
    #'%s/processed_data/RowleyShoals_SBE56Data.nc'%basedir,\
    '%s/processed_data/RowleyShoals2019_RDIADCPData_float64.nc'%basedir,\
    ]

# nctype = 3 is my format
nctype = 3
####

if create:
    print( 'Creating database: %s'%dbfile)
    netcdfio.createObsDB(dbfile)
    
for nc in ncfiles:
    print('Inserting metadata from: %s'%nc)    
    netcdfio.netcdfObs2DB(nc,dbfile, nctype=nctype)

print('Done.')
