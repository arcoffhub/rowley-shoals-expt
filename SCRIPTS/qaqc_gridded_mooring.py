"""
Calculate the mean density profile using all of the moorings combined
"""

import mycurrents.oceanmooring as om

from soda.utils.timeseries import timeseries
from soda.utils import harmonic_analysis
from soda.utils.othertime import datetime64todatetime, datetimetodatetime64

from iwaves import IWaveModes
from iwaves.utils.density import FitDensity, InterpDensity
import gsw

from scipy.interpolate import PchipInterpolator
import scipy.linalg as la
from scipy.optimize import newton_krylov

import mooring_utils

import xarray as xray
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import matplotlib.dates as mdates
import pandas as pd

import pdb

GRAV = 9.81
RHO0 = 1024.


#######
csvfile = 'Rowley2019_Groups.csv'

outfile = 'processed_data/RowleyShoals_Gridded_Mooring_T_20sec_QC.nc'
ncfile = 'processed_data/RowleyShoals_Gridded_Mooring_T_20sec.nc'

varname = 'Temperature'


plotting = True

########

def qaqc_temp(Tf, outfile, ncgroup, tstart, tend, writemode, plotting=False):


    ## Interpolate onto constant depths (not necessary for T only)
    #zout = np.median(T1.zvar, axis=1)
    #yi = T1.interp_z(zout,method='linear', fill_value=np.nan)
    #Tf = om.OceanMooring(T1.t, yi , -zout)
    
    ## Clip the first and last 12 hours
    #tstart = Tf.t[0] + np.timedelta64(12,'h')
    #tend = Tf.t[-1] - np.timedelta64(12,'h')
    Tf = Tf.clip(tstart, tend)

    #### QC
    ## NaN out when dT/dt is too large
    def qc_scalar(T, scalar, thresh=5.):
        mask = T.y.mask.copy()

        idx = scalar>thresh

        mask[idx] = True

        T.y[mask] = np.nan
        T.y = np.ma.MaskedArray(T.y, mask=mask)

    ##Tz = Tf.grad_z()
    ##qc_temp(Tf, Tz)
    #Tt = np.zeros_like(Tf.y)
    #Tt[:,1:] = np.diff(Tf.y,axis=1)
    #thresh = 5.
    #qc_scalar(Tf, np.abs(Tt), thresh=7.)

    # Remove data where the variance drops off (instrument has gone dead)
    # Compute the running variance of Tp to determine when the instrumnet goes bad
    print('Computing running variance...')
    Tp = timeseries(Tf.t, Tf.running_mean(Tf.y, dt=0.5*3600))
    Tpvar = timeseries(Tf.t, Tf.running_mean(np.power(Tf.y - Tp.y,2), dt=0.5*3600.))
    thresh = Tpvar.y<5e-7
    Tf.y[thresh] = np.nan

    #Tpvar_low = timeseries(Tf.t, np.cumsum(thresh,axis=1))
    ## Find the point when the number of points below the threshold is greater than 1500
    #for ii in range(Tf.Nz):
    #    idx = np.argwhere(Tpvar_low.y[ii,:]>1500)
    #    if idx.shape[0] > 0:
    #        Tf.y[ii,idx[0,0]::] = np.nan

    ## Remove section where the instruments are out of the water
    ## (based on variance of all instruments)
    #def remove_baddepth(Tf):
    #    var_z = np.nanvar(Tf.y, axis=0)
    #    var_mu = np.nanmedian(var_z)
    #    var_std = np.nanstd(var_z)
    #    var_bad = (var_z > var_mu+3*var_std) | (var_z < var_mu - 3*var_std)
    #    for ii in range(Tf.Nz):
    #        Tf.y[ii,var_bad] = np.nan

    #for ii in range(3):
    #    remove_baddepth(Tf)


        
    ## Remove rows where the gradient is wrong (i.e. instruments out of order)
    #Tdiff = np.zeros_like(Tf.y.data)

    #Tdiff[:-1, :] = np.diff(Tf.y,axis=0)
    #noverturns = np.nansum(Tdiff>0,axis=1)

    ##badrow = Tdiff > 0.
    #badrow = noverturns > Tf.Nt * 7

    # Check for anomalies
    for ii in range(3):
        mooring_utils.qc_temp(Tf, fac=5.)

    ## Remove bad rows by counting rows with > 70% NaNs
    #numbad = Tf.y.mask.sum(axis=1)
    #badidx = (numbad > Tf.Nt*0.7) | (badrow)
    #
    badidx = np.zeros((Tf.Nz,), dtype=np.bool)

    # Put everything backinto a masked array
    Tf.y = np.ma.MaskedArray(Tf.y, mask=np.isnan(Tf.y))

    #print('Found %d (out of %d) good instruments'%(np.sum(~badidx), Tf.Nz))

    Tout = om.OceanMooring(Tf.t, Tf.y[~badidx,:],\
        Tf.Z[~badidx],\
         units=Tf.units,\
         long_name=Tf.long_name,\
         StationID = Tf.StationID,\
         StationName = Tf.StationName,\
         varname = Tf.varname,\
         X= Tf.X,\
         Y= Tf.Y,\
         Z= Tf.Z,\
         positive=Tf.positive,\
         )

    if plotting:
        plt.figure()
        plt.imshow(Tf.y, extent=[0, Tf.Nt*Tf.dt,Tf.Z[-1],Tf.Z[0]],\
            aspect='auto', origin='upper')
        ##plt.pcolormesh(Tf.t, Tf.Z, Tf.y)
        plt.colorbar()
        #for ii,z in enumerate(Tout.Z):
        #    ax=plt.subplot(111)
        #    plt.plot(Tf.t, Tout.y[ii,:], lw=0.3)
        #    #plt.subplot(212, sharex=ax)
        #    #plt.plot(Tf.t, Tt[ii,:], lw=0.3)

        plt.show()

    Tout.to_netcdf(outfile, group=ncgroup,mode=writemode)
    print('Written to netcdf group %s...'%ncgroup)
    print(72*'#')
       

#####
ds = pd.read_csv(csvfile, index_col='group')

ncgroups = ds.index.values.tolist()

writemode = 'w'
for ncgroup in ncgroups:

    # For testing
    #if 'KIM400_2014' not in ncgroup:
    #    continue
    #if ncgroup not in 'PIL100_2012_a':
    #    continue

    print(72*'#')
    print('Processing group %s...'%ncgroup)
    print(72*'#')


    # Load the data
    #try:
    Tf = om.from_netcdf(ncfile, varname, group=ncgroup)
    t1 = datetime.strptime(ds['time_start'][ncgroup],'%d/%m/%Y %H:%M')
    t2 =  datetime.strptime(ds['time_end'][ncgroup],'%d/%m/%Y %H:%M')

    # Modify the depth to distance below surface
    zmin = -ds['depth'][ncgroup]
    Tf.Z += zmin
    Tf.positive='up'
    if '330' in ncgroup:
        # T330 is around the wrong way??
        Tf.Z = Tf.Z[::-1]
    qaqc_temp(Tf, outfile, ncgroup, t1, t2, writemode,plotting=plotting )
    #except:
    #    print('Group %s not found'%ncgroup)

    writemode='a'


        
