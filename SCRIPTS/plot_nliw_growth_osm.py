"""
Plot fitted buoyancy modes
"""

import xarray as xray
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
from scipy.signal import hilbert
import matplotlib.dates as mdates
from scipy.interpolate import splrep, splev

from iwaves.utils.minmax import get_peak_window
from soda.dataio.conversion.readotps import tide_pred

from mycurrents import oceanmooring as om
from netCDF4 import Dataset
omega = 1.42e-4

####


# Rowley Shoals data
ncfile = 'processed_data/RowleyShoals_Fitted_Buoyancy_Tonly_csv.nc'
sitename = 'T330' # 150, 200
xlims = [datetime(2019, 3, 6), datetime(2019, 4, 20)]
ylims = [-40,40]
outfile = 'FIGURES/RowleyShoals_IW_Amplitude_Timeseries_%s'%sitename
alphalims=[-0.008,0.008]
dAthresh = 0.08

## Browse 2017 data
##ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy_wout_motion_unvenfilt.nc'
##ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy_Tonly_csv.nc'
#ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy_Combined.nc'
#
#sitename = 'NP250' # 150, 200
#xlims = [datetime(2017, 4, 3), datetime(2017, 5, 8)]
#ylims = [-55,55]
#outfile = 'FIGURES/KISSME_IW_Amplitude_Timeseries_%s'%sitename
#alphalims = [-0.012,0.012]
#dAthresh = 0.10



# Tide stuff
modfile = '/home/suntans/Share/ScottReef/DATA/TIDES/TPXO7.2/Model_tpxo7.2'

mode = 1


plot = True
#####

# Find the appropriate groups in the processed file
nc = Dataset(ncfile)
sites = []
for site in nc.groups.keys():
    if sitename in site:
        sites.append(site)
nc.close()
print( sites)
ncgroups = sites
if len(sites) == 0:
    ncgroups = [None]



def get_tide(myds, lon, lat):
    h,u,v = tide_pred(modfile, np.array([lon]), np.array([lat]), ds.time.values)

    ssh = xray.DataArray(h.ravel(), coords={'time':myds.time.values}, dims=('time',))
    
    ## Get the RMS of the tidal elevation
    #SSH = timeseries(ssh.time.values, ssh.values)
    #SSHrms = timeseries(SSH.t, SSH.running_rms(SSH.y, 60*3600.))
    #
    #t,y = SSHrms.interp(myds.timeslow.values)

    #ssh_pd = pd.Series(y, index=t, name='SSH')

    
    return ssh# ssh_pd


def plotbox(xlims, ylims):
    plt.fill([xlims[0],xlims[1],xlims[1],xlims[0],xlims[0]],\
        [ylims[0],ylims[0],ylims[1],ylims[1],ylims[0]],\
        color='0.5',alpha=0.5)

# Plot formatting options
days = mdates.DayLocator()   # every year
hours = mdates.HourLocator()  # every month

if plot:
    plt.figure(figsize=(9,8))
    ax1=plt.subplot2grid((7,1),(0,0),rowspan=3) #N2
    ax1a=plt.subplot2grid((7,1),(3,0),rowspan=1, sharex=ax1) # alpha
    ax2=plt.subplot2grid((7,1),(4,0),rowspan=1, sharex=ax1) # ssh
    ax3=plt.subplot2grid((7,1),(5,0),rowspan=2, sharex=ax1) # Amax
    #ax4=plt.subplot2grid((7,1),(6,0),rowspan=1, sharex=ax1) # Growth rate
    #ax5=plt.subplot2grid((6,2),(3,1),rowspan=3)

for ncgroup in ncgroups:
    ds = xray.open_dataset(ncfile, group=ncgroup)
    try:
        lon,lat = float(ds.X), float(ds.Y)
    except:
        lon=123.
        lat=-14.
    

    ssh = get_tide(ds,lon,lat )

    if plot:
        plt.sca(ax1)
        z = ds.z.values
        time = ds.timeslow.values
        N = np.sqrt(ds.N2.values)
        rho = ds.rhobar.values

        plt.contour(time, z, rho.T, np.arange(1020, 1027., 0.25),\
                colors='k',linewidths=0.25)
        cc = plt.contourf(time, z, N.T, np.arange(0.0, 0.024, 0.002),\
             cmap='Blues', extend='max')
        cb=plt.colorbar(orientation='horizontal',shrink=0.3,pad=-0.15)
        cb.ax.set_title('N [s$^{-1}$]')
        cb.set_ticks([0,0.01,0.02])
        #ax0.set_xlim(tlims)
        #ax0.set_xticklabels([])
        plt.ylabel('Depth [m]')
        #plt.text(0.05,0.9,'(a)',transform=ax1.transAxes)


        # Plot u_bar
        plt.sca(ax2)
        #plt.plot(dsu.time, utidefit, '0.2', lw=0.5)
        ssh.plot(color='b', lw=0.5)
        #plt.plot(dsu.time, ubar_pr)
        #plt.plot(dsu.time, np.abs(uh), 'k', lw=0.5)

        plt.grid(b=True,ls=':')
        plt.xlim(xlims)
        plt.ylim(-3.0,3.0)
        plt.ylabel('$\eta$ [m]')
        #ax1.text(0,1.04,'(a)', transform=ax1.transAxes)
        #ax1.set_yticks([-0.4,-0.2,0,0.2,0.4])
        plt.xlabel('')

        # Plot alpha
        plt.sca(ax1a)
        alpha = -2*ds['r10']*ds['cn']
        #alpha = -np.abs(alpha)
        #alpha[:,0].plot(marker='.',ls='',color='b', ms=2.5)
        #alpha[:,mode].plot(marker='.',ls='',color='k', ms=2.5)
        plt.fill_between(alpha.timeslow, alpha[:,mode], color='0.5',alpha=0.5)

        plt.grid(b=True,ls=':')
        plt.xlim(xlims)
        plt.ylim(alphalims)
        plt.ylabel(r'$\alpha$ [s$^{-1}$]')
        #ax1a.text(0,1.04,'(b)', transform=ax1a.transAxes)
        #ax1.set_yticks([-0.4,-0.2,0,0.2,0.4])
        plt.xlabel('')
        plt.title('')


        # Plot Amp_M2 (mode 1 and 2)
        plt.sca(ax3)

        ds.A_n[:,mode].plot(color='k', lw=0.2)
        #ds.Atide[:,0].plot(ls='--', color='m', lw=0.5,alpha=0.5)
        #(ds.A_n[:,mode]-ds.Atide[:,0]).plot(color='b', lw=0.2)
        ds.amp[:,mode,0].plot(ls='--', color='b', lw=1.25)
        plt.legend(('Internal wave ($A$)','Internal tide ($a_0$)'),\
            bbox_to_anchor=(.01,1.25),loc='upper left')
        
        
        plt.ylabel('$A_{}$[m]'.format(mode+1))
        plt.xlabel('')
        plt.title('')
        plt.grid(b=True, ls=':')
        plt.xlim(xlims)
        plt.ylim(ylims)
        
        # Compute the gradient and mark the locations on the amplitude map
        print('Computing the time-rate of change...')
        raw = ds.A_n[:,mode]
        t = raw.time.values.astype(np.float)*1e-9
        f = splrep(t,raw.values,k=5,s=36000.)
        
        print('Done')
        raw_s = splev(t,f)
        dAdt_s=splev(t,f,der=1)
        idx = np.abs(dAdt_s)>dAthresh
        dAdt_c = 0*dAdt_s
        dAdt_c[~idx] = np.nan
        A_c = 1*raw_s
        A_c[~idx] = np.nan
        plt.plot(raw.time, dAdt_c,'r^',)

        
        
        
        #ax2.text(0,1.04,'(c)', transform=ax2.transAxes)
        #plotbox(xlims1, ylims)
        #plt.text(xlims1[1],40,'see (d)')

        #plt.sca(ax4)

        #Apr = np.abs(ds.A_n[:,mode]-ds.Atide[:,mode])
        #a0 = ds.amp[:,mode,0].sel(timeslow=ds.time, method='nearest')
        #idx = np.abs(ds.A_n[:,mode]) > 15.
        #biggrowth = ((Apr/a0)[idx])
        #biggrowth.plot(color='r',marker='.',ms=0.5,ls='')
        ##a0.plot()
        #plt.title('')
        #plt.grid(b=True)
        #plt.xlim(xlims)
        #plt.ylim(0,6)

        # Plot Am,alpha=0.5p_M2 (mode 1 and 2)
        ##plt.sca(ax3)

        ##ds.A_n[:,0].plot(color='b', lw=0.5)
        ##ds.Atide[:,0].plot(ls='--', color='m', lw=0.7)
        #plt.plot(ds.A_n.time.values,ds.A_n.values[:,0], color='b',lw=0.5)
        #plt.plot(ds.Atide.time.values,ds.Atide.values[:,0], ls='--', color='m', lw=0.7)
        ##plt.ylabel('Amplitude [m]')
        #plt.ylabel('Mode 1 - Amp [m]')
        #plt.title('')
        #plt.grid(b=True)
        #plt.xlim(xlims1)
        #plt.ylim(ylims)
        #ax3.text(0,1.04,'(d)', transform=ax3.transAxes)


        #days = mdates.DayLocator()   # every year
        #hours = mdates.HourLocator()  # every month
        #daysFmt = mdates.DateFormatter('%Y-%m-%d')
        #ax3.xaxis.set_major_locator(days)
        #ax3.xaxis.set_major_formatter(daysFmt)
        #ax3.xaxis.set_minor_locator(hours)


        ## Plot Amp_M2 (mode 1 and 2)
        #plt.sca(ax5)

        ##ds.A_n[:,1].plot(color='r', lw=0.5)
        #plt.plot(ds.A_n.time.values,ds.A_n.values[:,1], color='r',lw=0.5)
        #plt.plot(ds.Atide.time.values,ds.Atide.values[:,1], ls='--', color='m', lw=0.7)
        ##ds.Atide[:,1].plot(ls='--', color='m', lw=0.7)
        #plt.ylabel('Mode 2 - Amp [m]')
        #plt.title('')
        #plt.grid(b=True)
        #plt.xlim(xlims2)
        #plt.ylim(ylims)
        #ax5.text(0,1.04,'(e)', transform=ax5.transAxes)

        #days = mdates.DayLocator()   # every year
        #hours = mdates.HourLocator()  # every month
        #daysFmt = mdates.DateFormatter('%Y-%m-%d')
        #ax5.xaxis.set_major_locator(days)
        #ax5.xaxis.set_major_formatter(daysFmt)
        #ax5.xaxis.set_minor_locator(hours)

if plot:
    # Try and get some tick labels back
    #ax3.set_xticklabels(ax3.get_xticklabels(), visible=True)
    #ax4.set_xticklabels(ax4.get_xticklabels())

    days = mdates.DayLocator(interval=7)  # every year
    hours = mdates.DayLocator()  # every month
    daysFmt = mdates.DateFormatter('%Y-%m-%d')
    ax1.xaxis.set_major_locator(days)
    ax1.xaxis.set_major_formatter(daysFmt)
    ax1.xaxis.set_minor_locator(hours)


    
    plt.setp(ax1.get_xticklabels(), visible=False)
    plt.setp(ax1a.get_xticklabels(), visible=False)
    plt.setp(ax2.get_xticklabels(), visible=False)
    #plt.setp(ax3.get_xticklabels(), visible=False)
    plt.setp(ax3.get_xticklabels(), visible=True, rotation=12, ha='right')
    #ax4.set_xlabel('Time')


    #plt.tight_layout()
    plt.subplots_adjust(top=0.99, right=0.99, left=0.13, bottom=0.06 )
    #
    plt.savefig('%s.png'%outfile,dpi=150)
    plt.savefig('%s_dpi300.png'%outfile,dpi=300)
    plt.savefig('%s.pdf'%outfile,dpi=150)
    plt.show()


