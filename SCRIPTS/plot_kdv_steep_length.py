
# coding: utf-8

# In[1]:


import xarray as xray
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime
from scipy.signal import hilbert
import matplotlib.dates as mdates

import seaborn as sns
from scipy import stats

from iwaves.utils.minmax import get_peak_window

from iwaves import IWaveModes
from iwaves.utils.density import FitDensity

from mycurrents import oceanmooring as om

from soda.utils.timeseries import timeseries
from soda.dataio.conversion.readotps import tide_pred


from matplotlib import rcParams

import pdb



# Set font sizes
#rcParams['font.family'] = 'sans-serif'
#rcParams['font.sans-serif'] = ['Bitstream Vera Sans']
#rcParams['font.serif'] = ['Bitstream Vera Sans']
rcParams["font.size"] = "16"
rcParams['axes.labelsize']='large'

#%matplotlib inline


# In[2]:

## KISSME data
##dataset='KISMME'
#dataset='KISSME_uneven'
##ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy_wout_motion.nc'
#ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy_wout_motion_unvenfilt.nc'
#
#ncgroups = [\
#    #'WP250',
#    'NP250',
#    #'SP250',
#]
#
#outfiletmp = 'Docs/IW_Params_KISSME_mode%d.csv'


# Crux data
dataset = 'ROWLEY'
ncfile = '../ShellCrux/KP150_Fitted_Buoyancy_wout_motion_newtanh.nc'
ncfile = 'processed_data/RowleyShoals_Fitted_Buoyancy_Tonly_csv.nc'
ncgroups = [
    'T330',

]

outfiletmp = 'FIGURES/IW_Params_T330_mode%d.csv'

# Density params
zmax = -330
dz = 5

# Tide model params
modfile = '/home/suntans/Share/ScottReef/DATA/TIDES/TPXO7.2/Model_tpxo7.2'
lon = np.array([118.9750])
lat = np.array([-17.784])


ds = xray.open_dataset(ncfile, group=ncgroups[0])


# get a tide prediction
def get_tide(myds):
    h,u,v = tide_pred(modfile, lon, lat, ds.time.values)

    ssh = xray.DataArray(h.ravel(), coords={'time':myds.time.values}, dims=('time',))
    
    # Get the RMS of the tidal elevation
    SSH = timeseries(ssh.time.values, ssh.values)

    SSHrms = timeseries(SSH.t, SSH.running_rms(SSH.y, 60*3600.))
    
    
    t,y = SSHrms.interp(myds.timeslow.values)

    ssh_pd = pd.Series(y, index=t, name='SSH')

    
    return ssh, ssh_pd


# Find the max amplitude for each time period
def get_Amax(ds, mode):
    timeslow = ds.timeslow.to_index()
    Amax = np.zeros((timeslow.size,))
    ii=-1
    for t1, t2 in zip(timeslow[0:-1], timeslow[1::]):
        ii+=1
        A = ds.A_n.sel(time=slice(t1,t2))
        tmpmin = np.min(A[:,mode])
        tmpmax = np.max(A[:,mode])
        if np.abs(tmpmin) > tmpmax:
            tmp=tmpmin
        else:
            tmp=tmpmax
        if np.abs(tmp)>100.:
            Amax[ii]=Amax[ii-1]
        else:
            Amax[ii] = tmp
        #print t1, Amax[ii]    

    Amax_pd = pd.Series(Amax, index=timeslow, name='Amax')
    
    return Amax_pd


def get_data(mode, ngroup):

    ds = xray.open_dataset(ncfile, group=ncgroup)
    # Clip the KP150 data
    if ncgroup == 'KP150_phs1':
        ds = ds.sel(time=slice(datetime(2016,5,1), datetime(2016,9,15),None),
            timeslow=slice(datetime(2016,5,1), datetime(2016,9,15),None))

    #dsh, ssh_pd = get_tide(ds)
    Amax = xray.DataArray(get_Amax(ds, mode))
    a0 = ds['amp'][:,mode,0]
    r10 = ds['r10'][:,mode]
    c1 = ds['cn'][:,mode]

    data = xray.Dataset({'Amax':Amax,\
        'a0':a0, 'r10':r10, 'c1':c1})
        
    return data
# In[ ]:

# # Calculate the steepening time and compare
# 
# Steepening time definition:
# 
# $$
# \tau_s = \frac{1}{| 2 \pi \omega r_{10} a_0 |}
# $$

# In[ ]:



def plot_data(data, mode, colorbar=True):
    omega = 2*np.pi/(12.42*3600)
    tau_s = 1/(2*np.pi*omega*data['r10']*data['a0'])
    L_s = data['c1']*tau_s

    #tau_s.plot()
    #plt.show()
    if mode==1:
        xlim = [0,200]
        ylim = [-40,40]
        vmax = 20.
        
    if mode==0:
        xlim = [0,10000]
        ylim = [-40,40]
        vmax = 20.

    #plt.semilogx(np.abs(tau_s)/3600., data['Amax'],'.')
    plt.scatter(np.abs(L_s)/1000.,data['Amax'], c=data['a0'],             s=np.abs(data['Amax'])*2, cmap='Spectral_r',alpha=0.8, vmax=vmax)
    #plt.scatter(np.abs(tau_s)/86400., data['Amax'], \
    #            c=np.abs(data['Amax']), s=np.abs(data['Amax'])*3, cmap='Spectral_r',alpha=0.8, vmin=0,vmax=60)

    plt.grid(b=True)
    plt.ylabel('$A_{max}$ [m]')
    plt.xlabel(r'$L_s$ [km]')
    plt.gca().set_xscale('log')
    plt.xlim(xlim)
    plt.ylim(ylim)

    if colorbar:
        cb=plt.colorbar()
        cb.ax.set_title('$a_0$ [m]')

    #outfile = '../FIGURES/Amax_vs_Ls_%s_mode%d'%(dataset, mode+1)
    #print outfile
    #plt.savefig('%s.png'%outfile, dpi=90)
    #plt.savefig('%s.pdf'%outfile, dpi=90)

plt.figure(figsize=(9,6))
ax1=plt.subplot(121)
colorbar=True
for ncgroup in ncgroups:
    data1 = get_data(0, ncgroup)
    plot_data(data1,0, colorbar=colorbar)
    colorbar=False

ax2=plt.subplot(122)
colorbar=True
for ncgroup in ncgroups:
    data2 = get_data(1, ncgroup)
    plot_data(data2,1, colorbar=colorbar)
    colorbar=False


plt.text(0.9,0.9,'(a)', transform=ax1.transAxes)
plt.text(0.9,0.9,'(b)', transform=ax2.transAxes)
ax2.set_yticklabels([])
ax2.set_ylabel('')
#plt.tight_layout()

outfile = 'FIGURES/Amax_vs_Ls_%s'%(dataset)
print( outfile)
plt.savefig('%s.png'%outfile, dpi=90)
plt.savefig('%s.pdf'%outfile, dpi=90)


plt.show()



