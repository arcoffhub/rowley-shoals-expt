"""
Calculate the mean density profile using all of the moorings combined
"""

import mycurrents.oceanmooring as om

from soda.utils.timeseries import timeseries
from soda.utils import harmonic_analysis
from soda.utils.othertime import datetime64todatetime, datetimetodatetime64

from iwaves import IWaveModes
from iwaves.utils.density import FitDensity, InterpDensity
import gsw

from scipy.interpolate import PchipInterpolator
import scipy.linalg as la
from scipy.optimize import newton_krylov

import xarray as xray
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import matplotlib.dates as mdates
import pandas as pd

import pdb

GRAV = 9.81
RHO0 = 1024.


#######
csvfile = 'Rowley2019_Groups.csv'

outfile = 'processed_data/Rowley2019_Density_%s.csv'
#

ncfile = 'processed_data/RowleyShoals_Gridded_Mooring_T_20sec_QC.nc'

varname = 'Temperature'
dt = 20.
ntout = 6*3600.


dz_target = 5.

modes = np.arange(2)

plotting = True

########o

nt = int(ntout/dt)

def density_modes(Tf, outfile, zmin, Nz, writemode, plotting=False):


    ## Interpolate onto constant depths (not necessary for T only)
    #zout = np.median(T1.zvar, axis=1)
    #yi = T1.interp_z(zout,method='linear', fill_value=np.nan)
    #Tf = om.OceanMooring(T1.t, yi , -zout)
    ##Tf = Tf.clip(tstart, tend)
    ### QC

    ## NaN out start
    #def qc_temp(T, fac=3):
    #    mask = T.y.mask.copy()
    #    tmean = np.median(T.y, axis=1)
    #    tstd = T.y.std(axis=1)
    #    idx = (T.y.T > tmean + tstd*fac) & (T.y.T > tmean - tstd*fac) 
    #    mask[idx.T] = True

    #    idx2 = T.y < 2.
    #    mask[idx2] = True

    #    T.y[mask] = np.nan
    #    T.y = np.ma.MaskedArray(T.y, mask=mask)

    #qc_temp(Tf)
    #qc_temp(Tf)


    # Remove bad rows by counting rows with > 10% NaNs
    numbad = Tf.y.mask.sum(axis=1)
    badidx = numbad > Tf.Nt*0.7

    # Filter
    cutoff_t = 34*3600.
    #Tlow = om.OceanMooring(Tf.t, Tf.filt(cutoff_t, btype='low', order=5), Tf.Z)
    Tlow = om.OceanMooring(Tf.t, Tf.filt_uneven(cutoff_t, order=3)[~badidx,:],\
        Tf.Z[~badidx])

    time = Tlow.t
    t1,t2 = time[0],time[-1]

    ## Add on a surface measurement
    #Tsurf = om.OceanMooring(Tlow.t, Tlow.y[0,:], 0.)
    #Tlow.vstack(Tsurf)

     

    ## Plot the inputs
    if plotting:
        plt.figure()
        #plt.subplot(211)
        #Tf.contourf(np.arange(7,30,1))
        #Tf.contourf(np.arange(7,30,1), linestyles=':' , filled=False, cbar=False)
        #Tlow.contourf(np.arange(7,30,1), filled=False, cbar=False)
        #
        #
        #plt.subplot(212)
        Tlow.contourf(np.arange(7,30,1))
        plt.plot(np.repeat(Tlow.t[0],Tlow.Z.shape[0]),Tlow.Z,'k.')
        plt.show()

    Tlow_x = Tlow.to_xray()

    #t1s = [t0 - timedelta(hours=24) for t0 in time]
    #t2s = [t0 + timedelta(hours=24) for t0 in time]

    # define the dimensions
    time = Tlow.t[::nt]
    Nchunk = len(time)
    Nz = Nz

    #tstart,tend = T1.get_tslice(time[0]-timedelta(days=1), time[-1]+timedelta(days=1))
    #Nt = tend-tstart
    Nt = len(time)
    Nmode = len(modes)

    # Open the output file
    print('Writing to file %s'%outfile)
    f = open(outfile,'w')
    printstr = 'Time'
    for zz in Tlow.Z:
        printstr += ', %3.1f'%zz
    printstr += '\n'
    f.write(printstr)

    for ii,tt in enumerate(time):

        Zout = Tlow.Z #+ zmin
        #Tnow = Tlow_x.sel(time=tt).values
        Tnow = Tlow_x[:,ii*nt].values

        rho = gsw.pot_rho_t_exact(34.6*np.ones_like(Tnow), Tnow, -Zout, 0.)
        printstr = tt.astype(str)[:-10]
        for tmp in rho:
            printstr += ', %4.3f'%tmp
        printstr+='\n'
    
        f.write(printstr)

    f.close()
    print('Done')
        



#####
ds = pd.read_csv(csvfile, index_col='group')

ncgroups = ds.index.values.tolist()

writemode = 'w'
for ncgroup in ncgroups:

    ## For testing
    #if ncgroup not in 'PIL200_2012_a':
    #    continue
    #if 'KIM400' not in ncgroup:
    #    continue

    print(72*'#')
    print('Processing group %s...'%ncgroup)
    print(72*'#')

    zmin = -float(ds['depth'][ncgroup])
    #Nz = int(np.abs(zmin) // dz_target)+1
    dz = dz_target
    Z = np.arange(0, zmin - dz, -dz)
    Nz = Z.shape[0]
    #try:

    # Load the data
    try:
        Tf = om.from_netcdf(ncfile, varname, group=ncgroup)
        density_modes(Tf, outfile%ncgroup, zmin, Nz, writemode, plotting=plotting)
    except:
        print('Group %s not found'%ncgroup)


        
