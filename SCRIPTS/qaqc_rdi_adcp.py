
# coding: utf-8

# In[24]:

from mycurrents.adcpio import ADCP_io
from soda.dataio import mydbase

import xarray as xray
import numpy as np
import matplotlib.pyplot as plt


#######################
# Inputs

cutoff_angle = 20.
pgood_thresh = 80.
#errvel_thresh = 0.15
errvel_thresh = 2.5 # Make it high

condition = 'StationName LIKE "%RDI%" and Variable_Name == "pressure"'

dbfile = 'RowleyShoals2019_Obs.db'
table = 'observations'

outfile = 'processed_data/KISSME2017_RDI_QAQC.nc'


########################

# Query the database and get all of the sites
varnames = [\
        'NetCDF_Filename',\
        'NetCDF_GroupID',\
        'StationID',\
        'StationName',\
        ]

query = mydbase.returnQuery(dbfile, varnames, table, condition)
print(query)

mode='w'
for ncfile, group, station in zip(query['NetCDF_Filename'],\
        query['NetCDF_GroupID'], query['StationName']):
    
    # Change the orientation here
    if 'Sentinel' in station:
        orientation='down'
    else:
        orientation='up'

    print( 'Processing ADCP %s (orientation = %s)...'%(group, orientation))

    # Load the netcdf file
    adcp = ADCP_io(ncfile, group=group)

    adcp.qaqc_tilt(cutoff_angle)

    max_depth = np.abs(adcp.ds.Depth)

    #if 'Long' in station:
    #    P = 252-5.7 # Longranger P sensor is broken...
    #    corr_thresh = 110 #64
    #    echo_thresh = 25
    #else:
    P = None
    corr_thresh = 110
    echo_thresh = 30


    # Apply QA criteria to the raw data
    adcp.qaqc_depth(max_depth, orientation=orientation, P=P)

    adcp.qaqc_pgood(pgood_thresh)
    adcp.qaqc_errvel(errvel_thresh)

    adcp.qaqc_echo(echo_thresh)
    adcp.qaqc_corr(corr_thresh)

    adcp.ds['u'].attrs['coordinates'] = 'time zhat'
    adcp.ds['v'].attrs['coordinates'] = 'time zhat'
    adcp.ds['w'].attrs['coordinates'] = 'time zhat'
    adcp.ds['errvel'].attrs['coordinates'] = 'time zhat'

    # Rename the coordinates
    adcp.to_netcdf(outfile, group=group, mode=mode)

    mode='a'



#dsnew = process_adcp(ncfile, subsample_dt, max_depth, cutoff_angle)
#
## Get the lat/lon/depth info from the deployment database
#dsnew.attrs.update(
#        {'Longitude':query['Longitude'][0],
#        'Latitude':query['Latitude'][0],
#        'Depth':query['Depth'][0],
#        })
#
#
#dsnew.to_netcdf(outfile, group=group, mode=mode)

