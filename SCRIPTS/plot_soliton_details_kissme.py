"""
Plot the mooring data
"""

import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import numpy as np

from mycurrents import oceanmooring as om

from netCDF4 import Dataset
from datetime import datetime, timedelta
import pdb

def load_site(site, uvar, time1, time2):
    ######
    ##datadir = r'c:/Users/mrayson/cloudstor/Data/RowleyShoals2019'
    #datadir = 'processed_data/'
    ##adcpfile = '{}/RowleyShoals2019_RDI_QAQC.nc'.format(datadir)
    #adcpfile = '{}/RowleyShoals2019_RDIADCPData_float64.nc'.format(datadir)
    #Tfile = '{}/RowleyShoals_Gridded_Mooring_T_20sec_QC.nc'.format(datadir)

    #if site=='T330':
    #    group1 = 'T330_RDI_75kHz_LR_16870'
    #    group2 = None
    #    Tgroup = 'T330'

    #adcpfile = '../browse-basin-kissme/Data/NetCDF/KISSME2017_RDI_QAQC_noerrvel.nc'
    adcpfile = '../browse-basin-kissme/Data/NetCDF/KISSME2017_RDIADCPData.nc'
    #Tfile = 'Data/NetCDF/KISSME_Gridded_Mooring_TP_60sec.nc'
    Tfile = '../browse-basin-kissme/Data/NetCDF/KISSME_Gridded_Mooring_T_SBE56.nc'

    if site=='WP':
        group1 = 'WP250_RDI_150_kHz_Quartermaster_11795'
        group2 = 'WP250_RDI_300_kHz_Sentinel_20092'
        Tgroup = 'WP250'
    elif site=='SP':
        group1 = 'SP250_RDI75_kHz_Longranger_24613'
        group2 = None
        Tgroup = 'SP250'
    elif site=='NP':
        group1 = 'NP250_RDI_150_kHz_Quartermaster_16856'
        group2 = 'NP250_RDI_300_kHz_Monitor_20089'
        Tgroup = 'NP250'




    if uvar in ['u','v']:
        ulevs = np.arange(-0.8,0.9,0.10)
    elif uvar in ['w', 'errvel']:
        ulevs = np.arange(-0.2,0.22,0.02)


    #####
    # List the groups in the file
    #nc = Dataset(adcpfile)
    #for gg in nc.groups.keys():
    #    print gg
    #nc.close()

    # Load the moorings
    u1 = om.from_netcdf(adcpfile, uvar, group=group1)
    if group2 is not None:
        u2 = om.from_netcdf(adcpfile, uvar, group=group2)
    T = om.from_netcdf(Tfile, 'Temperature', group=Tgroup)

    T_p1 = T.clip(time1, time2)

    # Clip the data for plotting
    u_p1 = u1.clip(time1, time2)
    if group2 is not None:
        u_p2 = u2.clip(time1, time2)

    #u_p1.Z = -(u_p1.Z - 331.0)

    #T_p1.Z = -(T_p1.Z - 331.0)
    #T_p1.y = T_p1.y[::-1,:]
    #T_p1.Z -= 331
    #u_p1.Z -= 331
    T_p1.Z = -(T_p1.Z - 252.5)
    u_p1.Z = -(u_p1.Z - 252.5)
    return T_p1, u_p1

########
#
def rotate_uv(ubar, vbar, tide_angle):
    # Note this is a clockwise rotation matrix
    ubar_pr = ubar*np.cos(tide_angle) + vbar*np.sin(tide_angle)
    vbar_pr = -ubar*np.sin(tide_angle) + vbar*np.cos(tide_angle)
    return ubar_pr, vbar_pr


#t1 = datetime(2017,4,30,11,0,0)
#t2 = datetime(2017,4,30,13,0,0)

#t1 = datetime(2017,4,18,22,0,0)
#t2 = datetime(2017,4,19,0,0,0)

#t1 = datetime(2017,4,22,13,10,0)
#t2 = datetime(2017,4,22,13,40,0)

#t1 = datetime(2017,4,20,13,50,0)
#t2 = datetime(2017,4,20,14,20,0)

#t1 = datetime(2017,4,3,8,0,0)
#t2 = datetime(2017,4,3,9,0,0)

t1 = datetime(2017,5,6,7,42,0)
t2 = datetime(2017,5,6,8,12,0)

colorbar=True

outfile = 'FIGURES/SP250_mode2_example_{}'.format(t1.strftime('%Y%m%d'))
site = 'SP'
waveangle = -42

T, u = load_site(site,'u',t1,t2)
T, v = load_site(site,'v',t1,t2)
T, w = load_site(site,'w',t1,t2)
#Tnew = T.fill_gaps_z(kind='linear')

idx = ~np.any(T.y.mask, axis=1)
Tnew = om.OceanMooring(T.t, T.y[idx,:], T.Z[idx], )

tstack = [t1+timedelta(minutes=4) for ii in Tnew.Z]
#tstack = [t1+timedelta(minutes=0) for ii in Tnew.Z]

#Tnew.positive = 'down'

tide_angle = waveangle*np.pi/180.
u_rot, v_rot = rotate_uv(u.y, v.y, tide_angle)
U = u.copy_like(u.t, u_rot)
U.is2Dz = False
#U.Z = -(U.Z - 331.0)
w.is2Dz = False
#w.Z = -(w.Z - 331.0)

## Plot

myFmt = mdates.DateFormatter('%H:%M')

plt.figure(figsize=(6,5))
ax=plt.subplot(111, facecolor='0.9')
Tnew.contourf(np.arange(12,33,0.5), cbar=False, filled=False,linewidths=0.5)

ulevs = np.arange(-0.8,0.85,0.05)
C,cb = U.contourf(ulevs, cbar=colorbar, filled=True, cmap='RdBu')
if colorbar:
    cb.ax.set_title('u [m s$^{-1}$]')

plt.plot(tstack, Tnew.Z, 'kd', ms=1)

plt.ylim(250,25)
#plt.gca().invert_yaxis()
plt.ylabel('Depth [m]')
plt.grid(b=True)
#plt.xlabel('')
#ax.set_xticklabels([])
plt.xlabel('Time [HH:MM after %s]'%datetime.strftime(t1, '%d-%B-%Y'))
#plt.text(0.1,0.9,'(a)',transform=ax.transAxes)

#ax2=plt.subplot(212, facecolor='0.9')
#C,cb = Tnew.contourf(np.arange(12,33,0.5), cbar=False, filled=False,linewidths=0.5)
#
#wlevs = np.arange(-0.15,0.1625,0.0125)
#C,cb = w.contourf(wlevs, cbar=True, filled=True, cmap='PuOr')
#cb.ax.set_title('w [m s$^{-1}$]')
#
#plt.ylim(-330,-30)
##plt.gca().invert_yaxis()
#plt.ylabel('Depth [m]')
#plt.grid(b=True)
#
#plt.xticks(rotation=25)
#plt.xlabel('Time [HH:MM after %s]'%datetime.strftime(t1, '%d-%B-%Y'))
#ax2.xaxis.set_major_formatter(myFmt)
#
#plt.text(0.1,0.9,'(b)',transform=ax2.transAxes)
#
plt.tight_layout()

plt.savefig(outfile+'.png', dpi=150)
plt.savefig(outfile+'_300dpi.png', dpi=300)
plt.savefig(outfile+'.pdf', dpi=150)
plt.show()
