"""
Plot the nliw parameters

Calculated using: calc_iw_params_profiles.py
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime
from netCDF4 import Dataset

from iwaves import IWaveModes
from iwaves.utils.density import double_tanh_rho, single_tanh_rho
from soda.utils.mynumpy import grad_z
from soda.utils.otherplot import axcolorbar

import xarray as xray

###########

ncfile = 'processed_data/RowleyShoals_Fitted_Buoyancy_Tonly_csv.nc'

sitename = 'T200' # 100, 200

outfile = 'FIGURES/RowleyShoals_IW_N2_alpha_%s'%sitename

###########

# Load the parameters from the netcdf file
def plot_site(site, ax0, ax1, ax2, colorbar=True):
    ds = xray.open_dataset(ncfile, group=site)
    z = ds.z.values
    time = ds.timeslow.values
    N = np.sqrt(ds.N2.values)
    rho = ds.rhobar.values
    tlims = [time[0],time[-1]]

    alpha = -2*ds['r10']*ds['cn']

    plt.sca(ax0)
    plt.contour(time, z, rho.T, np.arange(1020, 1027., 0.25),\
            colors='k',linewidths=0.25)
    cc = plt.contourf(time, z, N.T, np.arange(0.0, 0.024, 0.002),\
         cmap='Blues', extend='max')
    #plt.colorbar()
    #ax0.set_xlim(tlims)
    #ax0.set_xticklabels([])
    plt.ylabel('Depth [m]')
    plt.text(0.05,0.9,'(a)',transform=ax0.transAxes)

    #####
    plt.sca(ax2)
    ds['cn'][:,0].plot(color='k')
    ds['cn'][:,1].plot(color='r')
    plt.legend(('Mode 1','Mode 2'), loc='upper right')
    (0*alpha[:,0]).plot(color='0.5',ls='--',lw=1,)
    #plt.plot(params['r10']*0,'r--')
    #plt.ylabel('$r_{10}$ [m$^{-1}$]')
    plt.ylabel(r'$c_n$ [m s$^{-1}$]')
    #ax1.set_yscale('log')
    #ax1.set_xticklabels([])
    #ax2.set_xlim(tlims)
    #if mode==0:
    #    ax1.set_ylim(-5e-3,5e-3)
    #elif mode==1:
    ax2.set_ylim(0.0,1.8)
    plt.xticks(rotation=17.)
    plt.text(0.05,0.8,'(b)',transform=ax2.transAxes)
    plt.title('')
    plt.grid(b=True)



    plt.sca(ax1)
    # Plot a time series of each variable
    alpha[:,0].plot(marker='.',ls='',color='k')
    alpha[:,1].plot(marker='.',ls='',color='r')
    #plt.legend(('Mode 1','Mode 2'), loc='lower right')
    (0*alpha[:,0]).plot(color='0.5',ls='--',lw=1,)
    #plt.plot(params['r10']*0,'r--')
    #plt.ylabel('$r_{10}$ [m$^{-1}$]')
    plt.ylabel(r'$\alpha$ [s$^{-1}$]')
    #ax1.set_yscale('log')
    #ax1.set_xticklabels([])
    #ax1.set_xlim(tlims)
    #if mode==0:
    #    ax1.set_ylim(-5e-3,5e-3)
    #elif mode==1:
    ax1.set_ylim(-0.008,0.008)
    plt.xticks(rotation=17.)
    plt.text(0.05,0.8,'(c)',transform=ax1.transAxes)
    plt.title('')
    plt.grid(b=True)

    #ax2 = plt.subplot2grid((5,1), (3,0), rowspan=1, sharex=ax0)
    #plt.plot(params['r20'])
    #plt.plot(params['r20']*0,'r--')
    #plt.ylabel('$r_{20}$ [s m$^{-3}$]')
    #ax2.set_xticklabels([])
    #ax2.set_xlim(tlims)
    #plt.text(0.05,0.9,'(c)',transform=ax2.transAxes)
    #
    #ax3 = plt.subplot2grid((5,1), (4,0), rowspan=1,)
    #plt.plot(params['c1'])
    ##plt.plot(params['c1']*0,'r--')
    #plt.ylabel('$c_{1}$ [m s$^{-1}$]')
    #ax3.set_xlim(tlims)
    #plt.text(0.05,0.9,'(d)',transform=ax3.transAxes)
    #plt.xticks(rotation=17.)

    #plt.tight_layout()

    #plt.savefig('%s.png'%outfile, dpi=150)
    #plt.savefig('%s.pdf'%outfile, dpi=150)
    return cc

### 
# Find the appropriate groups in the processed file
nc = Dataset(ncfile)
sites = []
for site in nc.groups.keys():
    if sitename in site:
        sites.append(site)
nc.close()
print( sites)

##
# Plotting
plt.figure(figsize=(7,9))
ax0 = plt.subplot2grid((5,1), (0,0), rowspan=3)
ax1 = plt.subplot2grid((5,1), (4,0), rowspan=1, )
ax2 = plt.subplot2grid((5,1), (3,0), rowspan=1, )


for site in sites:
    cc = plot_site(site, ax0, ax1, ax2,)

# KISSME period
ax0.set_xlim(datetime(2019, 3, 6), datetime(2019, 4, 20))

ax1.set_xlim(ax0.get_xlim())
ax2.set_xlim(ax0.get_xlim())

ax0.set_xticklabels([])
ax2.set_xticklabels([])

ax0.set_xlabel('')
ax2.set_xlabel('')
#cb=axcolorbar(cc, ax=ax2, pos =[0.15, 0.85, 0.26, 0.08])
cb=axcolorbar(cc, ax=ax0, pos =[0.15, 0.10, 0.26, 0.04])
cb.ax.set_title('N(z) [s$^{-1}$]')
cb.set_ticks([0,0.01,0.02])

plt.savefig('%s.png'%outfile, dpi=150)
plt.savefig('%s.pdf'%outfile, dpi=150)

plt.show()





