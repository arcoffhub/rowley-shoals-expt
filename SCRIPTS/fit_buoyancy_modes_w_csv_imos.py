##

###

import mycurrents.oceanmooring as om

from soda.utils.timeseries import timeseries
from soda.utils import harmonic_analysis
from soda.utils.othertime import datetime64todatetime, datetimetodatetime64

from iwaves import IWaveModes
from iwaves.utils.density import FitDensity, InterpDensity
from iwaves.utils.fitting import fit_bmodes_linear_w_iw
import gsw

from scipy.interpolate import PchipInterpolator, interp1d
import scipy.linalg as la
from scipy.optimize import newton_krylov

import xarray as xray
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import matplotlib.dates as mdates
import pandas as pd

import mooring_utils

import pdb

GRAV = 9.81
RHO0 = 1024.

def fit_modes(site, zmin, Nz, ncfile, dsrho, outfile, write_mode):
    ncgroup = site

    maxmodes = 5

    varname = 'Temperature'
    
    # Load some data
    T = om.from_netcdf(ncfile, varname, group=ncgroup)

    freqs = ['M2','M4','M6']

    plotting = False
    plotpath = 'FIGURES/BuoyancyModes'

    ###########

    # QC the raw temp
    #mooring_utils.qc_temp(T)
    #mooring_utils.qc_temp(T)

    # Work out the number of suitable modes based on the instrument spacing

    if T.Z[0] > T.Z[-1]:
        # We need to flip
        signdz = 1
        spacing = -1
    else:
        signdz = -1
        spacing = 1

    dz = signdz*2*np.diff(T.Z)
    nmodes = min(int(np.floor(zmin/dz).min()), maxmodes)
    modes = range(nmodes)
    print('Fitting %d modes (based on instrument spacing\n'%nmodes)

    # Get the time from the QC'd csv file
    time = dsrho.time.values


    # define the dimensions
    Nz = Nz
    Nfreq = len(freqs)
    freqvals, frqnames = harmonic_analysis.getTideFreq(freqs)

    tstart,tend = T.get_tslice(time[0], time[-1])

    Nt = tend - tstart
    Nmode = len(modes)
    Nchunk = time.shape[0]

    # Define the coordinates:
    coords = {
        'time':T.t[tstart:tend],
        'timeslow': datetimetodatetime64(time),
        'modes':np.array(modes),
        'z': np.linspace(zmin,0, Nz)[::-1], # top to bottom
        'freqs':freqvals,
        }

    # Create the output variable as xray.DataArray objects
    A_nt = xray.DataArray(np.zeros((Nt, Nmode)),
            dims = ('time','modes'),
            coords = {'time':coords['time'], 'modes':coords['modes']},
            attrs = {'long_name':'Modal buoyancy amplitude',
                    'units':'m',
                    }
           )

    rhofit_t = xray.DataArray(np.zeros((Nt, Nz)),
            dims = ('time','z'),
            coords = {'time':coords['time'], 'z':coords['z']},
            attrs = {'long_name':'Best-fit density',
                    'units':'kg m^-3',
                    }
           )

    Atide = xray.DataArray(np.zeros((Nt, Nmode)),
            dims = ('time','modes'),
            coords = {'time':coords['time'],'modes':coords['modes']},
            attrs = {'long_name':'Tidal fit amplitude',
                    'units':'m',
                    }
           )

    rho_t = xray.DataArray(np.zeros((Nchunk, Nz)),
            dims = ('timeslow','z'),
            coords = {'timeslow':coords['timeslow'], 'z':coords['z']},
            attrs = {'long_name':'Background density',
                    'units':'kg m^-3',
                    }
           )

    N2_t = xray.DataArray(np.zeros((Nchunk, Nz)),
            dims = ('timeslow','z'),
            coords = {'timeslow':coords['timeslow'], 'z':coords['z']},
            attrs = {'long_name':'Backgrouna buoyancy frequency squared',
                    'units':'s^-2',
                    }
           )

    cn_t = xray.DataArray(np.zeros((Nchunk, Nmode)),
            dims = ('timeslow','modes'),
            coords = {'timeslow':coords['timeslow'], 'modes':coords['modes']},
            attrs = {'long_name':'Linear phase speed',
                    'units':'m s^-1',
                    }
           )

    r10_t = xray.DataArray(np.zeros((Nchunk, Nmode)),
            dims = ('timeslow','modes'),
            coords = {'timeslow':coords['timeslow'], 'modes':coords['modes']},
            attrs = {'long_name':'Nonlinear steepening parameter',
                    'units':'m^-1',
                    }
           )

    phi_t = xray.DataArray(np.zeros((Nchunk, Nmode, Nz)),
            dims = ('timeslow','modes','z'),
            coords = {'timeslow':coords['timeslow'], 'modes':coords['modes'], 'z':coords['z']},
            attrs = {'long_name':'Modal structure function',
                    'units':'',
                    }
           )

    amp_t = xray.DataArray(np.zeros((Nchunk,Nmode, Nfreq)),
            dims = ('timeslow', 'modes', 'freqs'),
            coords = {'timeslow':coords['timeslow'],\
                    'modes':coords['modes'],\
                    'freqs':coords['freqs']},
            attrs = {'long_name':'Mode-one harmonic amplitude',
                    'units':'kg m^-3',
                    }
           )

    phs_t = xray.DataArray(np.zeros((Nchunk,Nmode, Nfreq)),
            dims = ('timeslow', 'modes', 'freqs'),
            coords = {'timeslow':coords['timeslow'],\
                    'modes':coords['modes'],\
                    'freqs':coords['freqs']},
            attrs = {'long_name':'Mode-one harmonic phase',
                    'units':'radians',
                    }
           )

    # Initialise some output arrays

    #for t0 in time:
    #    t1 = t0 - timedelta(hours=24)
    #    t2 = t0 + timedelta(hours=24)
    #for t1, t2 in zip(t1s, t2s):
    ii=0
    for t1 in time[ii:-8]:

        t2 = time[ii+8]
        Tbar = dsrho[ii+4,:].values
        #Tbar = dsrho[ii,:].values
        ii+=1

        # Get the index for inserting the new time series in the output array
        ti1,ti2 = T.get_tslice(t1, t2)
        if ii == 0:
            tstart = ti1
        ti1 -= tstart
        ti2 -= tstart
        print(t1, ti1, ti2)

        ZT = dsrho.depth.values
        #Tshort = T.y[:,ti1:ti2]
        Tshort = T.clip(t1,t2)

        #Tshort.contourf(np.arange(5,32,1));plt.show()

        ### Calculate the density time series
        Temp = Tshort.y
        mask = Tshort.y.mask
        #Z = -Tshort.Z
        #Z = ZT
        Z = T.Z

        nz,nt = Temp.shape
        S = 34.6*np.ones((nz,nt))
        P = -Z[...,np.newaxis]*np.ones((1,nt))

        rho = gsw.pot_rho_t_exact(S, Temp, P, 0.)
        rho = rho.data
        #rhobar = rho.mean(axis=1)

        # Preserve the mask
        rho[mask]=np.nan

        ## get the density from the pre-loaded timeseries
        #dsnow = ds.sel(timeslow=time[ii], method='nearest')

        #iw = iwavemodes(dsnow.rhobar.values[::-1], dsnow.z.values[::-1],\
        #        density_class=interpdensity)

        # Fit the density again
        if nz < 10:
            density_func = 'single_tanh'
        else:
            density_func = 'double_tanh_new'
            
        iw = IWaveModes(Tbar[::spacing], ZT[::spacing], \
                density_class=FitDensity, density_func=density_func ) 

        #A_t, phi, rhofit, rhofitall, iw, r10, c1 = fit_bmodes_linear_wgaps(rho, Z, \
        A_t, rhofitall, phi, c1, r10 = mooring_utils.fit_bmodes_linear_wgaps(\
            rho[::spacing,:], Z[::spacing], \
            zmin, modes, iw, Nz=Nz)

        # check for bad fits
        badA = A_t > 0.8*np.abs(zmin)
        A_t[badA] = 0.
        #if np.abs(A_t).max() > 0.75*np.abs(zmin):
        #    print('Warning -- unrealistic amplitudes detected. Setting to NaN...')
        #    A_t[:] = np.nan

        #A_t, phi, rhofit = fit_bmodes_linear(rho, rhobar, Z, modes,\
        #    full_output=False)

        # Nonlinear fit
        #Anl, rhofitnl, iw = fit_bmodes_nonlinear(rho, rhobar, Z, 0)

        # Compute harmonics of A (mode-one only)
        #An = om.OceanMooring(Tshort.t, A_t[0,:].squeeze(), 0.)
        An = om.OceanMooring(Tshort.t, A_t, np.arange(Nmode))
        #freqs = ['M2']
        amp, phs, frq, mean, yfit, yrms = An.tidefit(frqnames=freqs,\
                basetime=datetime(2017,1,1))

        print( 'An_max: ', A_t.max())
        print( 'amp_max: ', amp.max())
        #print( 'phs diff: ',phs-phs[0])

        # Convert to an oceanmooring object for plotting
        rho_om = om.OceanMooring(Tshort.t, rho, Z)
        #rhofit_om = om.OceanMooring(Tshort.t, rhofit, Z)
        rhofit_om = om.OceanMooring(Tshort.t, rhofitall, iw.Z)
        #rhofitnl_om = om.OceanMooring(Tshort.t, rhofitnl, Z)

        # Update the DataArray objects
        A_nt[ti1:ti2,:] = A_t
        rhofit_t[ti1:ti2,:] = rhofitall
        Atide[ti1:ti2,:] = yfit
        rho_t[ii,:] = iw.rhoZ
        N2_t[ii,:] = iw.N2
        phi_t[ii,:,:] = phi.T

        r10_t[ii,...] = r10
        cn_t[ii,...] = c1

        amp_t[ii,...] = amp
        phs_t[ii,...] = phs

        # Check the density fit is OK
        #plt.figure()
        #plt.plot(rhofitall.mean(axis=0), iw.Z,'r')
        #plt.plot(iw.rhoZ, iw.Z,'k')
        #plt.plot(iw.rho, iw.z,'k+')
        #plt.plot(rho.mean(axis=1),Z,'r+')
        #plt.show()


        ####
        #
        if plotting:
            fig = plt.figure(figsize=(10,7))
            ax=plt.subplot(311)
            rho_om.contourf(clevs=np.arange(1020, 1028.,0.5), cbar=False)
            rho_om.contourf(clevs=np.arange(1020, 1028.,0.5), filled=False, cbar=False)
            plt.ylabel('Depth [m]')
            ax.set_xlim(t1,t2)
            ax.set_xticklabels([])

            ax1= plt.subplot(312, sharex=ax)
            rhofit_om.contourf(clevs=np.arange(1020, 1028.,0.5), cbar=False)
            rhofit_om.contourf(clevs=np.arange(1020, 1028.,0.5), filled=False, cbar=False)
            plt.ylabel('Depth [m]')
            ax.set_xlim(t1,t2)
            ax1.set_xticklabels([])

            ax2= plt.subplot(313, sharex=ax)
            plt.plot(Tshort.t, A_t[:,0], 'b', lw=0.5)
            plt.plot(Tshort.t, A_t[:,1], 'r', lw=0.5)

            plt.plot(An.t, yfit[:,0],'m--')
            plt.legend(('Mode 1','Mode 2','Harmonic'), loc='upper left')
            #plt.plot(An.t, Anl,'k', lw=0.7) # Nonlinear mode-1
            ax2.set_xlim(t1,t2)
            ax2.set_ylim(-55,55)
            plt.ylabel('Amplitude [m]')
            plt.grid(b=True)

            myFmt = mdates.DateFormatter('%d-%b %H:%M')
            ax2.xaxis.set_major_formatter(myFmt)
            plt.xticks(rotation=17)

            plt.tight_layout()

            #outplot = '%s_%s_%s_%s.png'%(plotpath, ncgroup, t1.strftime('%Y%m%d%H%M'),
            #    t2.strftime('%Y%m%d%H%M'))
            outplot = '%s/%s_%s_%s.png'%(plotpath, ncgroup, str(t1)[:13],str(t2)[:13])



            print( outplot)
            fig.savefig(outplot, dpi=150)
            #plt.show()

            del fig

    # Create a dataset object and save
    #A_nt[ti1:ti2,:] = A_t.T
    #rhofit_t[ti1:ti2,:] = rhofitall
    #rho_t[ii,:] = iw.rhoZ
    #N2_t[ii,:] = iw.N2
    #phi_t[ii,:,:] = phi.T
    #amp_t[ii,:] = amp
    #phs_t[ii,:] = phs

    ds = xray.Dataset({
            'A_n':A_nt,
            'rhofit':rhofit_t,
            'Atide':Atide,
            'rhobar':rho_t,
            'N2':N2_t,
            'phi':phi_t,
            'amp':amp_t,
            'phs':phs_t,
            'r10':r10_t,
            'cn':cn_t,
        },
        attrs={
            'ncfile':ncfile,
            'group':ncgroup,
            'Description':'Linear vertical mode fit to KISSME mooring data',
            'X':T.X,\
            'Y':T.Y,
        })


    if write_mode is not None:
        print( 'Writing to file: %s...'%outfile)
        ds.to_netcdf(outfile, mode=write_mode, format='NETCDF4', group=ncgroup)
        print('Done')

    #Atide.plot();plt.show()

#########
#

csvfile = 'Rowley2019_Groups.csv'

densityfilestr = 'processed_data/Rowley2019_Density_%s.csv'

ncfile = 'processed_data/RowleyShoals_Gridded_Mooring_T_20sec_QC.nc'
#

outfile = 'processed_data/RowleyShoals_Fitted_Buoyancy_Tonly_csv.nc'

write_mode = 'w'

dz_target = 5.0

######


ds = pd.read_csv(csvfile, index_col='group')

sites = ds.index.values.tolist()

for site in sites[:]:
    #if 'PIL200' not in site:
    #    continue
    print(72*'#')
    print('Site: ',site)
    print(72*'#')

    # Load the background density data
    try:
        densitycsv = densityfilestr%site
        dsrho = mooring_utils.read_density_csv(densitycsv)

        zmin = -float(ds['depth'][site])
        #Nz = int(np.abs(zmin) // dz_target)+1
        dz = dz_target
        Z = np.arange(0, zmin - dz, -dz)
        Nz = Z.shape[0]
    except:
        print('Bad csv file. Skipping...')
        continue


    if dsrho.depth.shape[0] <= 4:
        print('Skipping site %s, only %d stations...'%(site, dsrho.depth.shape[0]))
        continue
 
    fit_modes(site, zmin, Nz, ncfile, dsrho,outfile , write_mode)
    write_mode = 'a'



