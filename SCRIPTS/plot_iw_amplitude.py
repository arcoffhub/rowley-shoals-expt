"""
Plot fitted buoyancy modes
"""

import xarray as xray
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
from scipy.signal import hilbert
import matplotlib.dates as mdates

from iwaves.utils.minmax import get_peak_window
from soda.dataio.conversion.readotps import tide_pred

from mycurrents import oceanmooring as om
from netCDF4 import Dataset
omega = 1.42e-4

def print_max_params(ds, time, mode):
    tstr = str(tt)[0:19].replace('T',' ')
    amp = ds.A_n.sel(time=time).values[mode]
    amptide = ds.Atide.sel(time=time).values[mode]
    #ubar = uh.sel(time=time, method='nearest').values
    r10 = ds.r10.sel(timeslow=time, method='nearest').values[mode]
    cn = ds.cn.sel(timeslow=time, method='nearest').values[mode]
    if mode ==0:
        a0 = 30.
    else:
        a0 = 10.
    tau_s = 1/(omega*a0*np.abs(r10))

    print( '%s, %3.1f, %3.1f, %3.1f, %1.6f, %1.2f, %4.1f'%\
        ( tstr, amp, amptide, amp/amptide, r10, cn, tau_s/86400. ))

####


ncfile = 'processed_data/RowleyShoals_Fitted_Buoyancy_Tonly_csv.nc'

sitename = 'T330' # 150, 200

xlims = [datetime(2019, 3, 6), datetime(2019, 4, 20)]

ylims = [-50,50]

# Tide stuff
modfile = '/home/suntans/Share/ScottReef/DATA/TIDES/TPXO7.2/Model_tpxo7.2'
lon = np.array([118.9750])
lat = np.array([-17.784])


outfile = 'FIGURES/RowleyShoals_IW_Amplitude_Timeseries_%s'%sitename


plot = True
#####

# Find the appropriate groups in the processed file
nc = Dataset(ncfile)
sites = []
for site in nc.groups.keys():
    if sitename in site:
        sites.append(site)
nc.close()
print( sites)
ncgroups = sites



def get_tide(myds):
    h,u,v = tide_pred(modfile, lon, lat, ds.time.values)

    ssh = xray.DataArray(h.ravel(), coords={'time':myds.time.values}, dims=('time',))
    
    ## Get the RMS of the tidal elevation
    #SSH = timeseries(ssh.time.values, ssh.values)
    #SSHrms = timeseries(SSH.t, SSH.running_rms(SSH.y, 60*3600.))
    #
    #t,y = SSHrms.interp(myds.timeslow.values)

    #ssh_pd = pd.Series(y, index=t, name='SSH')

    
    return ssh# ssh_pd


def plotbox(xlims, ylims):
    plt.fill([xlims[0],xlims[1],xlims[1],xlims[0],xlims[0]],\
        [ylims[0],ylims[0],ylims[1],ylims[1],ylims[0]],\
        color='0.5',alpha=0.5)

# Plot formatting options
days = mdates.DayLocator()   # every year
hours = mdates.HourLocator()  # every month

if plot:
    plt.figure(figsize=(9,8))
    ax1=plt.subplot2grid((6,1),(0,0),rowspan=1)
    ax1a=plt.subplot2grid((6,1),(1,0),rowspan=1, sharex=ax1)
    ax2=plt.subplot2grid((6,1),(2,0),rowspan=2, sharex=ax1)
    #ax3=plt.subplot2grid((6,2),(0,1),rowspan=3)
    ax4=plt.subplot2grid((6,1),(4,0),rowspan=2, sharex=ax1)
    #ax5=plt.subplot2grid((6,2),(3,1),rowspan=3)

for ncgroup in ncgroups:
    ds = xray.open_dataset(ncfile, group=ncgroup)

    if ncgroup == 'KP150_phs1':
        ds = ds.sel(time=slice(datetime(2016,5,1), datetime(2016,9,15),None))


    #### Extract the barotropic tide
    #dsu = xray.open_dataset(uvfile, group=ncgroup[0:2])
    #
    #ubar = dsu.Au_n[:,0].values
    #vbar = dsu.Av_n[:,0].values

    ## Rotate along the main axis
    #tide_angle = -42*np.pi/180.

    ## Note this is a clockwise rotation matrix
    #ubar_pr = ubar*np.cos(tide_angle) + vbar*np.sin(tide_angle)
    #vbar_pr = -ubar*np.sin(tide_angle) + vbar*np.cos(tide_angle)

    #ubar_pr[np.abs(ubar_pr)>10.] = 0.

    #U = om.OceanMooring(dsu.time.values, ubar_pr, 0.)
    #V = om.OceanMooring(dsu.time.values, vbar_pr, 0.)

    #utide = U.tidefit(['M2','S2','K1','O1','P1','Q1','N2','K2'])
    #vtide = V.tidefit(['M2','S2','K1','O1','P1','Q1','N2','K2'])

    #utidefit = utide[-2]
    #vtidefit = vtide[-2]

    ## hilbert transform the get the amplitude
    #uh = hilbert(utidefit)
    #uh = om.OceanMooring(dsu.time.values, uh, 0.).to_xray()
    ssh = get_tide(ds)

    # Find the max mode 1 and 2 waves
    numwaves = 10
    ## Find the peak mode-one waves of depression
    mode = 1
    wavefunc = 'max'
    window=3

    peaks, tpeakm2, ctr = get_peak_window(ds.A_n[:,mode].values, ds.time.values,\
                    360, numwaves, fun=wavefunc, ctr=0, vals=[], idxs=[])

    peak_mode2= ds.A_n.sel(time=tpeakm2)[:,mode].values

    mode = 0
    wavefunc = 'min'
    window=3

    peaks, tpeakm1, ctr = get_peak_window(ds.A_n[:,mode].values, ds.time.values,\
                    360, numwaves, fun=wavefunc, ctr=0, vals=[], idxs=[])

    peak_mode1= ds.A_n.sel(time=tpeakm1)[:,mode].values

    print( 'Mode 1:')
    print( 'time,A,A_tide,scale,ubar,r10,cn,tau_s')
    for tt in tpeakm1: 
        print_max_params(ds, tt, 0)


    print( 'Mode 2:')
    print( 'time,A,A_tide,scale,ubar,r10,cn,tau_s')
    for tt in tpeakm2: 
        print_max_params(ds, tt, 1)

    if plot:
        # Plot u_bar
        plt.sca(ax1)
        #plt.plot(dsu.time, utidefit, '0.2', lw=0.5)
        ssh.plot(color='0.2', lw=0.2)
        #plt.plot(dsu.time, ubar_pr)
        #plt.plot(dsu.time, np.abs(uh), 'k', lw=0.5)

        plt.grid(b=True)
        plt.xlim(xlims)
        plt.ylim(-3.0,3.0)
        plt.ylabel('$\eta$ [m]')
        ax1.text(0,1.04,'(a)', transform=ax1.transAxes)
        #ax1.set_yticks([-0.4,-0.2,0,0.2,0.4])
        plt.xlabel('')

        # Plot alpha
        plt.sca(ax1a)
        alpha = -2*ds['r10']*ds['cn']
        alpha[:,0].plot(marker='.',ls='',color='b', ms=2.5)
        alpha[:,1].plot(marker='.',ls='',color='r', ms=2.5)

        plt.grid(b=True)
        plt.xlim(xlims)
        plt.ylim(-0.008,0.008)
        plt.ylabel(r'$\alpha$ [s$^{-1}$]')
        ax1a.text(0,1.04,'(b)', transform=ax1a.transAxes)
        #ax1.set_yticks([-0.4,-0.2,0,0.2,0.4])
        plt.xlabel('')
        plt.title('')




        # Plot Amp_M2 (mode 1 and 2)
        plt.sca(ax2)

        #ds.A_n[:,0].plot(color='b', lw=0.2)
        #ds.Atide[:,0].plot(ls='--', color='m', lw=0.5,alpha=0.5)
        (ds.A_n[:,0]-ds.Atide[:,0]).plot(color='b', lw=0.2)
        ds.amp[:,0,0].plot(ls='--', color='0.2', lw=1.25)
        plt.ylabel('Mode 1 - Amp [m]')
        plt.xlabel('')
        plt.title('')
        plt.grid(b=True)
        plt.xlim(xlims)
        plt.ylim(ylims)
        ax2.text(0,1.04,'(c)', transform=ax2.transAxes)
        #plotbox(xlims1, ylims)
        #plt.text(xlims1[1],40,'see (d)')
        plt.plot(tpeakm1, peak_mode1 ,'kd')

        # Plot Am,alpha=0.5p_M2 (mode 1 and 2)
        ##plt.sca(ax3)

        ##ds.A_n[:,0].plot(color='b', lw=0.5)
        ##ds.Atide[:,0].plot(ls='--', color='m', lw=0.7)
        #plt.plot(ds.A_n.time.values,ds.A_n.values[:,0], color='b',lw=0.5)
        #plt.plot(ds.Atide.time.values,ds.Atide.values[:,0], ls='--', color='m', lw=0.7)
        ##plt.ylabel('Amplitude [m]')
        #plt.ylabel('Mode 1 - Amp [m]')
        #plt.title('')
        #plt.grid(b=True)
        #plt.xlim(xlims1)
        #plt.ylim(ylims)
        #ax3.text(0,1.04,'(d)', transform=ax3.transAxes)


        #days = mdates.DayLocator()   # every year
        #hours = mdates.HourLocator()  # every month
        #daysFmt = mdates.DateFormatter('%Y-%m-%d')
        #ax3.xaxis.set_major_locator(days)
        #ax3.xaxis.set_major_formatter(daysFmt)
        #ax3.xaxis.set_minor_locator(hours)

        # Plot Amp_M2 (mode 1 and 2)
        plt.sca(ax4)
        #ds.A_n[:,1].plot(color='r', lw=0.2)
        #ds.Atide[:,1].plot(ls='--', color='m', lw=0.3,alpha=0.5)
        (ds.A_n[:,1]-ds.Atide[:,1]).plot(color='r', lw=0.2)
        ds.amp[:,1,0].plot(ls='--', color='0.2', lw=1.25)
        plt.ylabel('Mode 2 - Amp [m]')
        plt.title('')
        plt.grid(b=True)
        plt.xlim(xlims)
        plt.ylim(ylims)
        ax4.text(0,1.04,'(d)', transform=ax4.transAxes)
        #plotbox(xlims2, ylims)
        #plt.text(xlims2[1],-40,'see (e)')

        plt.plot(tpeakm2, peak_mode2 ,'kd')

        ## Plot Amp_M2 (mode 1 and 2)
        #plt.sca(ax5)

        ##ds.A_n[:,1].plot(color='r', lw=0.5)
        #plt.plot(ds.A_n.time.values,ds.A_n.values[:,1], color='r',lw=0.5)
        #plt.plot(ds.Atide.time.values,ds.Atide.values[:,1], ls='--', color='m', lw=0.7)
        ##ds.Atide[:,1].plot(ls='--', color='m', lw=0.7)
        #plt.ylabel('Mode 2 - Amp [m]')
        #plt.title('')
        #plt.grid(b=True)
        #plt.xlim(xlims2)
        #plt.ylim(ylims)
        #ax5.text(0,1.04,'(e)', transform=ax5.transAxes)

        #days = mdates.DayLocator()   # every year
        #hours = mdates.HourLocator()  # every month
        #daysFmt = mdates.DateFormatter('%Y-%m-%d')
        #ax5.xaxis.set_major_locator(days)
        #ax5.xaxis.set_major_formatter(daysFmt)
        #ax5.xaxis.set_minor_locator(hours)

if plot:
    # Try and get some tick labels back
    #ax3.set_xticklabels(ax3.get_xticklabels(), visible=True)
    #ax4.set_xticklabels(ax4.get_xticklabels())
    
    plt.setp(ax1.get_xticklabels(), visible=False)
    plt.setp(ax1a.get_xticklabels(), visible=False)
    plt.setp(ax2.get_xticklabels(), visible=False)
    plt.setp(ax4.get_xticklabels(), visible=True, rotation=30, ha='right')
    ax4.set_xlabel('Time')


    plt.tight_layout()
    #
    plt.savefig('%s.png'%outfile,dpi=150)
    plt.savefig('%s.pdf'%outfile,dpi=150)
    plt.show()


